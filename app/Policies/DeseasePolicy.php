<?php

namespace App\Policies;

use App\Models\User;
use App\Models\UserRight;
use Illuminate\Auth\Access\HandlesAuthorization;

class DeseasePolicy
{
    use HandlesAuthorization;

    const MODEL = 'admin';

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user)
    {
        return $this->checkRight($user, 'view');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user)
    {
        return $this->checkRight($user, 'update');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user)
    {
        return $this->checkRight($user, 'delete');
    }

    private function checkRight(User $user, string $right) {
        return UserRight::where('user_id', $user->id)
            ->where('right', $right)
            ->where('model', self::MODEL)
            ->exists();
    }
}
