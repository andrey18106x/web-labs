<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Desease extends Model
{
    use HasFactory;

    protected $table = 'deseases';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'mortality',
        'patient_id',
    ];

    public function patient() {
        return $this->belongsTo(Patient::class, 'patient_id', 'id');
    }
}
