<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;


class DashboardController extends Controller {

    public function index() {
        return view('dashboard', [
            'patients' => \App\Models\Patient::all(),
            'deseases' => \App\Models\Desease::all(),
        ]);
    }

}