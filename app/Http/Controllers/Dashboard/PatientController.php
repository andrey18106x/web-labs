<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Desease;
use App\Models\Patient;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\User;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /** @var User $user */
        $user = Auth::user();
        if ($user && $user->can('view', Patient::class)) {
            return view('patients', [
                'patients' => \App\Models\Patient::all()
            ]);
        } else {
            return view('not-permited');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /** @var User $user */
        $user = Auth::user();
        if ($user && $user->can('update', Patient::class)) {
            return view('patients/create');
        } else {
            return view('not-permited');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();
        if ($user && $user->can('update', Patient::class)) {
            $request->validate([
                'patient-name' => ['required', 'max:28'],
                'patient-surname' => ['required', 'max:28'],
                'patient-middle-name' => ['required', 'max:28'],
                'patient-address' => ['required', 'max:255'],
                'patient-birthday' => ['required'],
            ], [
                'patient-name.required' => 'Patient first name required!',
                'patient-name.max' => 'Patient first name length must be less then 28 symbols',
                'patient-surname.required' => 'Patient last name required!',
                'patient-surname.max' => 'Patient last name length must be less then 28 symbols',
                'patient-middle-name.required' => 'Patient middle name required!',
                'patient-middle-name.max' => 'Patient middle name length must be less then 28 symbols',
                'patient-address.required' => 'Patient address required!',
                'patient-address.max' => 'Patient address length must be less then 255 symbols',
                'patient-birthday.required' => 'Patient birthday date required!'
            ]);

            $patient = new \App\Models\Patient();
            $patient->name = \request('patient-name');
            $patient->surname = \request('patient-surname');
            $patient->middle_name = \request('patient-middle-name');
            $patient->address = \request('patient-address');
            $patient->birthday = \request('patient-birthday');

            $patient->save();

            return redirect('/dashboard/patients');
        } else {
            return view('not-permited');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $patient_id
     * @return \Illuminate\Http\Response
     */
    public function show($patient_id)
    {
        /** @var User $user */
        $user = Auth::user();
        if ($user && $user->can('view', Patient::class)) {
            return view('patients/show', [
                'patient' => \App\Models\Patient::find($patient_id),
            ]);
        } else {
            return view('not-permited');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function edit($patient_id)
    {
        /** @var User $user */
        $user = Auth::user();
        if ($user && $user->can('update', Patient::class)) {
            return view('patients/edit', [
                'patient' => \App\Models\Patient::find($patient_id)
            ]);
        } else {

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $patient_id
     * @return \Illuminate\Http\Response
     */
    public function update($patient_id)
    {
        /** @var User $user */
        $user = Auth::user();
        if ($user && $user->can('update', Patient::class)) {
            $patient = \App\Models\Patient::find($patient_id);
            $patient->name = \request('patient-name');
            $patient->surname = \request('patient-surname');
            $patient->middle_name = \request('patient-middle-name');
            $patient->address = \request('patient-address');
            $patient->birthday = \request('patient-birthday');
            $patient->save();
            return redirect('/dashboard/patients');
        } else {
            return view('not-permited');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $patient_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($patient_id)
    {
        /** @var User $user */
        $user = Auth::user();
        if ($user && $user->can('delete', Patient::class)) {
            $patient = \App\Models\Patient::find($patient_id);
            /** @var Desease */
            foreach ($patient->deseases as $desease) {
                $desease->delete();
            }
            $patient->delete();
            return redirect('/dashboard/patients');
        } else {
            return view('not-permited');
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getList() {
        /** @var User $user */
        $user = Auth::user();
        if ($user && $user->can('update', Patient::class)) {
            return \App\Models\Patient::all();
        } else {
            return view('not-permited');
        }
    }
}
