<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Desease;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class DeseasesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($patient_id = null) {
        /** @var User $user */
        $user = Auth::user();
        if ($user && $user->can('view', Desease::class)) {
            return view('deseases', [
                'patient' => $patient_id == null ? null : \App\Models\Patient::find($patient_id),
                'patients' => \App\Models\Patient::all(),
                'deseases' => $patient_id == null ? \App\Models\Desease::all() : \App\Models\Patient::find($patient_id)->deseases
            ]);
        } else {
            return view('not-permited');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        /** @var User $user */
        $user = Auth::user();
        if ($user && $user->can('update', Desease::class)) {
            return view('deseases/create', [
                'patients' => \App\Models\Patient::all(),
            ]);
        } else {
            return view('not-permited');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        /** @var User $user */
        $user = Auth::user();
        if ($user && $user->can('update', Desease::class)) {
            $request->validate([
                'desease-name' => ['required', 'max:128'],
                'desease-mortality' => ['required', 'numeric', 'min:1', 'max:50'],
                'desease-description' => ['max:255'],
            ], [
                'desease-name.required' => 'Desease name must be filled!',
                'desease-name.max' => 'Desease name length must be no more than 128 symbols',
                'desease-mortality.required' => 'Desease mortality must be specified',
                'desease-mortality.min' => 'Desease mortality must be higher than 0',
                'desease-mortality.max' => 'Desease mortality must be no more than 50',
                'desease-description.max' => 'Desease description length should be less than 255 symbols'
            ]);

            $desease = new \App\Models\Desease();
            $desease->name = \request('desease-name');
            $desease->mortality = \request('desease-mortality');
            $desease->description = \request('desease-description');
            $desease->patient_id = \request('desease-patient-id');
            $desease->save();

            return redirect('/dashboard/deseases');
        } else {
            return view('not-permited');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $desease_id
     * @return \Illuminate\Http\Response
     */
    public function show($desease_id) {
        /** @var User $user */
        $user = Auth::user();
        if ($user && $user->can('view', Desease::class)) {
            $desease = \App\Models\Desease::find($desease_id);
            return view('deseases/show', [
                'desease' => $desease,
                'patient' => \App\Models\Patient::find($desease->patient_id),
            ]);
        } else {
            return view('not-permited');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $desease_id
     * @return \Illuminate\Http\Response
     */
    public function edit($desease_id) {
        /** @var User $user */
        $user = Auth::user();
        if ($user && $user->can('update', Desease::class)) {
            $desease = \App\Models\Desease::find($desease_id);
            return view('deseases/edit', [
                'desease' => $desease,
                'patient' => $desease->patient,
                'patients' => \App\Models\Patient::all(),
            ]);
        } else {
            return view('not-permited');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $desease_id
     * @return \Illuminate\Http\Response
     */
    public function update($desease_id) {
        /** @var User $user */
        $user = Auth::user();
        if ($user && $user->can('update', Desease::class)) {
            $desease = \App\Models\Desease::find($desease_id);
            $desease->name = \request('desease-name');
            $desease->mortality = \request('desease-mortality');
            $desease->description = \request('desease-description');
            $desease->patient_id = \request('desease-patient-id');
            $desease->save();
            return redirect('/dashboard/deseases');
        } else {
            return view('not-permited');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $desease_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($desease_id) {
        /** @var User $user */
        $user = Auth::user();
        if ($user && $user->can('delete', Desease::class)) {
            $desease = \App\Models\Desease::find($desease_id);
            $desease->delete();
            return redirect('/dashboard/deseases');
        } else {
            return view('not-permited');
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getList() {
        return \App\Models\Desease::all();
    }
}