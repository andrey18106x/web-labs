<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

class UsersController extends Controller {

    public function index() {
        if (Gate::allows('admin-panel')) {
            return view('users/home', [
                'users' => \App\Models\User::all()
            ]);
        } else {
            return view('not-permited');
        }
    }

    public function create() {
        if (Gate::allows('admin-panel')) {
            return view('users/create');
        } else {
            return view('not-permited');
        }
    }

    public function store() {
        if (Gate::allows('admin-panel')) {
            \request()->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'confirmed', Rules\Password::defaults()],
            ]);

            $user = User::create([
                'name' => \request('name'),
                'email' => \request('email'),
                'password' => Hash::make(\request('password')),
                'role' => \request('role'),
            ]);

            event(new Registered($user));

            return redirect('dashboard/users');
        } else {
            return view('not-permited');
        }
    }

    public function show($user_id) {
        if (Gate::allows('admin-panel')) {
            return view('users/show', [
                'user' => \App\Models\User::find($user_id)
            ]);
        } else {
            return view('not-permited');
        }
    }

    public function edit($user_id) {
        if (Gate::allows('admin-panel')) {
            return view('users/edit', [
                'user' => \App\Models\User::find($user_id)
            ]);
        } else {
            return view('not-permited');
        }
    }

    public function update($user_id) {
        if (Gate::allows('admin-panel')) {
            $user = \App\Models\User::find($user_id);
            $user->name = \request('user-name');
            $user->email = \request('user-email');
            $user->role = \request('user-role');
            $user->save();
            return redirect('dashboard/users');
        } else {
            return view('not-permited');
        }
    }

    public function destroy($user_id) {
        if (Gate::allows('admin-panel')) {
            $user = \App\Models\User::find($user_id);
            foreach($user->rights as $right) {
                $right->delete();
            }
            $user->delete();
            return redirect('dashboard/users');
        } else {
            return view('not-permited');
        }
    }

}