<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppVariantTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->default('');
            $table->string('surname')->default('');
            $table->string('middle_name')->default('');
            $table->string('address')->default('');
            $table->date('birthday');
        });

        Schema::create('deseases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('mortality');
            $table->string('description')->default('');
            $table->unsignedBigInteger('patient_id');

            $table->foreign('patient_id')->references('id')->on('patients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
        Schema::dropIfExists('deseases');
    }
}
