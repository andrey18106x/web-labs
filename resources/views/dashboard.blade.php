<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    @if (count($patients) > 0)
                        <h3 class="my-3 border-solid border-b-1 border-gray-500 border-opacity-100">
                            <a href="/dashboard/patients">{{ __('Patients') }}</a> ({{ count($patients) }})
                        </h3>
                    @else
                        <h3>{{ __('No patients') }}</h3>
                    @endif
                </div>
                <div class="p-6 bg-white border-b border-gray-200">
                    @if (count($deseases) > 0)
                        <h3 class="my-3 border-solid border-b-1 border-gray-500 border-opacity-100">
                            <a href="/dashboard/deseases">{{ __('Deseases') }}</a> ({{ count($deseases) }})
                        </h3>
                    @else
                        <h3>{{ __('No deseases') }}</h3>
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
