<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit user') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form action="/dashboard/users/{{ $user->id }}" method="post" class="flex flex-col w-1/2 m-auto">
                        @csrf
                        {{ method_field("patch") }}
                        <div class="input-group my-3">
                            <x-label for="user-name" :value="__('User name')" />
                            <x-input id="user-name" class="block mt-1 w-full" type="text" name="user-name" :value="$user->name" :placeholder="__('User name')" />
                        </div>
                        <div class="input-group my-3">
                            <x-label for="user-email" :value="__('User email')" />
                            <x-input id="user-email" class="block mt-1 w-full" type="text" name="user-email" :value="$user->email" :placeholder="__('User email')" />
                        </div>
                        <div class="input-group my-3">
                            <x-label for="user-role" :value="__('User role')" />
                            <x-input id="user-role" class="block mt-1 w-full" type="text" name="user-role" :value="$user->role" :placeholder="__('User role')" />
                        </div>
                        @if ($errors->any())
                            <ul class="validation-errors p-3 my-3 border border-1 border-red-400 rounded">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        <div class="actions flex justify-between items-center mt-3">
                            <x-button>
                                <span class="icon mr-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" viewBox="0 0 20 20" fill="currentColor">
                                        <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                    </svg>
                                </span>
                                <span class="text">{{ __('Update') }}</span>
                            </x-button>
                            <a href="/dashboard/users/">
                                <x-button type="button">
                                    <span class="icon mr-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" viewBox="0 0 20 20" fill="currentColor">
                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd" />
                                        </svg>
                                    </span>
                                    <span class="text">{{ __('Cancel') }}</span>
                                </x-button>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
