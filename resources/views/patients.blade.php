<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Patients') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <a href="/dashboard/patients/create">
                <x-button type="button" class="bg-green-400 text-white mb-3">
                    <span class="icon mr-2">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M10 3a1 1 0 011 1v5h5a1 1 0 110 2h-5v5a1 1 0 11-2 0v-5H4a1 1 0 110-2h5V4a1 1 0 011-1z" clip-rule="evenodd" />
                        </svg>
                    </span>
                    <span class="text flex items-center justify-center">{{ __('Add new patient') }}</span>
                </x-button>
            </a>
            <div class="bg-white overflow-hidden overflow-y-auto max-h-96 shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    @if (count($patients) > 0)
                        @foreach ($patients as $patient)
                            <div class="desease-item flex items-center justify-between border-1 border-b border-gray-200 py-2 px-2">
                                <p>{{ $patient->id }}. {{ $patient->surname }} {{ $patient->name }} {{ $patient->middle_name }}</p>
                                <div class="actions flex justify-between items-center">
                                    <a href="/dashboard/patients/{{ $patient->id }}" class="mr-2">
                                        <x-button>
                                            <span class="icon flex items-center mr-2">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" viewBox="0 0 20 20" fill="currentColor">
                                                    <path d="M10 12a2 2 0 100-4 2 2 0 000 4z" />
                                                    <path fill-rule="evenodd" d="M.458 10C1.732 5.943 5.522 3 10 3s8.268 2.943 9.542 7c-1.274 4.057-5.064 7-9.542 7S1.732 14.057.458 10zM14 10a4 4 0 11-8 0 4 4 0 018 0z" clip-rule="evenodd" />
                                                </svg>
                                            </span>
                                            <span class="text">{{ __('View') }}</span>
                                        </x-button>
                                    </a>
                                    <a href="/dashboard/patients/{{ $patient->id }}/edit" class="mr-2">
                                        <x-button>
                                            <span class="icon flex items-center mr-2">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
                                                </svg>
                                            </span>
                                            <span class="text">{{ __('Edit') }}</span>
                                        </x-button>
                                    </a>
                                    <x-button data-id="{{ $patient->id }}" data-target="patients" data-token="{{ csrf_token() }}" class="action-delete">
                                        <span class="icon flex items-center mr-2">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" viewBox="0 0 20 20" fill="rgba(239, 68, 68, 0.9)">
                                                <path fill-rule="evenodd" d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z" clip-rule="evenodd" />
                                            </svg>
                                        </span>
                                        <span class="text text-red-500">{{ __('Delete') }}</span>
                                    </x-button>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <p class="text-gray-500">{{ __('Patients list is empty.') }}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
