<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Viewing existing Patient') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white px-3 overflow-hidden shadow-sm sm:rounded-lg flex justify-center">
                <div class="w-full py-6 px-3 bg-white border-b border-gray-200">
                    @if ($patient !== null)
                    <form action="/dashboard/patients/{{ $patient->id }}" method="post" class="flex flex-col w-1/2 m-auto">
                        @csrf
                        {{ method_field("patch") }}
                        <div class="input-group my-3">
                            <x-label for="patient-surname" :value="__('Patient last name')" />
                            <x-input id="patient-surname" class="block mt-1 w-full" type="text" name="patient-surname" :value="$patient->surname" :placeholder="__('Patient last name')" disabled />
                        </div>
                        <div class="input-group my-3">
                            <x-label for="patient-name" :value="__('Patient first name')" />
                            <x-input id="patient-name" class="block mt-1 w-full" type="text" name="patient-name" :value="$patient->name" :placeholder="__('Patient first name')" disabled />
                        </div>
                        <div class="input-group my-3">
                            <x-label for="patient-middle-name" :value="__('Patient middle name')" />
                            <x-input id="patient-middle-name" class="block mt-1 w-full" type="text" name="patient-middle-name" :value="$patient->middle_name" :placeholder="__('Patient middle name')" disabled />
                        </div>
                        <div class="input-group my-3">
                            <x-label for="patient-address" :value="__('Patient address')" />
                            <x-input id="patient-address" class="block mt-1 w-full" type="text" name="patient-address" :value="$patient->address" :placeholder="__('Patient address')" disabled />
                        </div>
                        <div class="input-group my-3">
                            <x-label for="patient-birthday" :value="__('Patient birthday')" />
                            <x-input id="patient-birthday" class="block mt-1 w-full" type="date" name="patient-birthday" :value="$patient->birthday" :placeholder="__('Patient birthday')" disabled />
                        </div>
                        <div class="actions flex justify-between items-center mt-3">
                            <a href="/dashboard/patients">
                                <x-button type="button">
                                    <span class="icon mr-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" viewBox="0 0 20 20" fill="currentColor">
                                            <path fill-rule="evenodd" d="M9.707 14.707a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 1.414L7.414 9H15a1 1 0 110 2H7.414l2.293 2.293a1 1 0 010 1.414z" clip-rule="evenodd" />
                                        </svg>
                                    </span>
                                    <span class="text">{{ __('Back') }}</span>
                                </x-button>
                            </a>
                            <x-button data-id="{{ $patient->id }}" data-token="{{ csrf_token() }}" class="action-delete">
                                <span class="icon flex items-center mr-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" viewBox="0 0 20 20" fill="rgba(239, 68, 68, 0.9)">
                                        <path fill-rule="evenodd" d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z" clip-rule="evenodd" />
                                    </svg>
                                </span>
                                <span class="text text-red-500">{{ __('Delete') }}</span>
                            </x-button>
                        </div>
                    </form>
                    @else
                        <p>Desease not found</p>
                        <p>{{ json_encode($desease) }}</p>
                    @endif
                </div>
                @include('patients/deseases')
            </div>
        </div>
    </div>
</x-app-layout>
