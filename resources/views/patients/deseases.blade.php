<div class="w-1/2 m-6 rounded py-3 px-3 bg-white border border-gray-200">
    @if (count($patient->deseases) > 0)
        <h4 class="my-1">{{ __('Patient Deseases') }}</h4>
        @foreach ($patient->deseases as $desease)
            <p>
                <a class="underline px-3 py-2" href="/dashboard/deseases/{{ $desease->id }}">{{ $desease->id }}. {{ $desease->name }} ({{ __('mortality: ') }}{{ $desease->mortality }})</a>
            </p>
        @endforeach
    @else
        <p>{{ __('Patient has no deseases') }}</p>
        <p><a class="underline" href="/dashboard/deseases/create">{{ __('Create a new desease') }}</a></p>
    @endif
</div>