<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Viewing existing Desease') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    @if ($desease !== null)
                        <form action="/dashboard/deseases/{{ $desease->id }}" method="post" class="flex flex-col w-1/2 m-auto">
                            @csrf
                            {{ method_field("patch") }}
                            <h3>
                                <a class="underline" href="/dashboard/patients/{{ $patient->id }}">{{ __('Patient: ') }} {{ $patient->surname }} {{ $patient->name }} {{ $patient->middle_name }}</a>
                            </h3>
                            <div class="input-group my-3">
                                <x-label for="desease-name" :value="__('Desease name')" />
                                <x-input id="desease-name" class="block mt-1 w-full" type="text" name="desease-name" :value="$desease->name" :placeholder="__('Desease name')" disabled />
                            </div>
                            <div class="input-group my-3">
                                <x-label for="desease-mortality" :value="__('Desease mortality')" />
                                <x-input id="desease-mortality" class="block mt-1 w-full" type="number" name="desease-mortality" :value="$desease->mortality" disabled />
                            </div>
                            <div class="input-group my-3">
                                <x-label for="desease-description" :value="__('Desease description')" />
                                <x-input id="desease-description" class="block mt-1 w-full" type="text" name="desease-description" :value="$desease->description" disabled />
                            </div>
                            <div class="actions flex justify-between items-center mt-3">
                                <a href="/dashboard/deseases">
                                    <x-button type="button">
                                        <span class="icon mr-2">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" viewBox="0 0 20 20" fill="currentColor">
                                                <path fill-rule="evenodd" d="M9.707 14.707a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 1.414L7.414 9H15a1 1 0 110 2H7.414l2.293 2.293a1 1 0 010 1.414z" clip-rule="evenodd" />
                                            </svg>
                                        </span>
                                        <span class="text">{{ __('Back') }}</span>
                                    </x-button>
                                </a>
                                <x-button data-id="{{ $desease->id }}" data-token="{{ csrf_token() }}" class="action-delete">
                                    <span class="icon flex items-center mr-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" viewBox="0 0 20 20" fill="rgba(239, 68, 68, 0.9)">
                                            <path fill-rule="evenodd" d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z" clip-rule="evenodd" />
                                        </svg>
                                    </span>
                                    <span class="text text-red-500">{{ __('Delete') }}</span>
                                </x-button>
                            </div>
                        </form>
                    @else
                        <p>{{ __('Desease not found') }}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
