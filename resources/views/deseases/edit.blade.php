<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Editing existing Desease') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form action="/dashboard/deseases/{{ $desease->id }}" method="post" class="flex flex-col w-1/2 m-auto">
                        @csrf
                        {{ method_field("patch") }}
                        <div class="input-group my-3">
                            <x-label for="desease-name" :value="__('Desease name')" />
                            <x-input id="desease-name" class="block mt-1 w-full" type="text" name="desease-name" :value="$desease->name" :placeholder="__('Desease name')" autofocus />
                        </div>
                        <div class="input-group my-3">
                            <x-label for="desease-mortality" :value="__('Desease mortality')" />
                            <x-input id="desease-mortality" class="block mt-1 w-full" type="number" name="desease-mortality" :value="$desease->mortality" />
                        </div>
                        <div class="input-group my-3">
                            <x-label for="desease-description" :value="__('Desease description')" />
                            <x-input id="desease-description" class="block mt-1 w-full" type="text" name="desease-description" :value="$desease->description" />
                        </div>
                        <div class="input-group my-3">
                            @if (count($patients) > 0)
                                <x-label for="desease-patient" :value="__('Desease patient')" />
                                <x-select name="desease-patient-id" id="desease-patient">
                                    @foreach ($patients as $patient_l)
                                        <option value="{{ $patient_l->id }}" :selected="{{ $patient_l->id === $patient->id }}">{{ $patient_l->id }}. {{ $patient_l->surname }} {{ $patient_l->name }}</option>
                                    @endforeach
                                </x-select>
                            @else
                                <p>{{ __('No patients.') }} <a class="underline" href="/dashboard/patients/create">{{ __('Create a new one') }}</a></p>
                            @endif
                        </div>
                        @if ($errors->any())
                            <ul class="validation-errors p-3 my-3 border border-1 border-red-400 rounded">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        <div class="actions flex justify-between items-center mt-3">
                            <x-button>
                                <span class="icon mr-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" viewBox="0 0 20 20" fill="currentColor">
                                        <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                    </svg>
                                </span>
                                <span class="text">{{ __('Update') }}</span>
                            </x-button>
                            <a href="/dashboard/deseases">
                                <x-button type="button">
                                    <span class="icon mr-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" viewBox="0 0 20 20" fill="currentColor">
                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd" />
                                        </svg>
                                    </span>
                                    <span class="text">{{ __('Cancel') }}</span>
                                </x-button>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>