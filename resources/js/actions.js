document.addEventListener('DOMContentLoaded', () => {

    const actionDeleteButtons = document.querySelectorAll('.action-delete');

    if (actionDeleteButtons.length > 0) {
        actionDeleteButtons.forEach(actionDeleteButton => {
            actionDeleteButton.addEventListener('click', (e) => {
                e.preventDefault();
                const targetId = actionDeleteButton.dataset.id;
                const target = actionDeleteButton.dataset.target;
                window.axios.post(`/dashboard/${target}/${targetId}`, {
                    _method: 'delete',
                    _token: actionDeleteButton.dataset.token
                }).then(res => {
                    if (res.status === 200) {
                        location.href = `/dashboard/${target}`;
                    }
                }).catch(err => {
                    console.debug(err);
                    alert('Some error occured while deleting desease record');
                });
            });
        });
    }

    const deseasesFilter = document.querySelector('select[name="patient-filter"]');

    if (deseasesFilter !== null) {
        console.log(deseasesFilter);
        deseasesFilter.addEventListener('change', () => {
            const filterValue = deseasesFilter.options[deseasesFilter.selectedIndex].value;
            if (filterValue > 0) {
                location.assign(`/dashboard/deseases/patient/${filterValue}`);
            } else {
                location.assign(`/dashboard/deseases`);
            }
        });
    }

});