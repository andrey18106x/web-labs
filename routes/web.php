<?php

use App\Http\Controllers\Dashboard\DashboardController;
use App\Http\Controllers\Dashboard\DeseasesController;
use App\Http\Controllers\Dashboard\PatientController;
use App\Http\Controllers\Dashboard\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', [DashboardController::class, 'index'])
->name('dashboard');

Route::resource('/dashboard/deseases', DeseasesController::class)->names([
    'index' => 'deseases',
    'create' => 'deseases/create',
    'show' => 'deseases/show',
    'edit' => 'deseases/edit',
])->middleware(['auth']);
Route::get('/dashboard/deseases/patient/{id}', [DeseasesController::class, 'index'])
->middleware(['auth', 'VerifiedUser'])->name('deseases/filter');

Route::get('/deseases-json', [DeseasesController::class, 'getList']);

Route::resource('/dashboard/patients', PatientController::class)->names([
    'index' => 'patients',
    'create' => 'patients/create',
    'show' => 'patients/show',
    'edit' => 'patients/edit',
])->middleware(['auth', 'VerifiedUser']);

Route::resource('/dashboard/users', UsersController::class)->names([
    'index' => 'users',
    'create' => 'users/create',
    'show' => 'users/show',
    'edit' => 'users/edit',
])->middleware(['auth', 'VerifiedUser']);

// Route::get('/dashboard/users', [UsersController::class, 'index'])
// ->middleware(['auth', 'VerifiedUser'])->name('dashboard/users');

require __DIR__.'/auth.php';
